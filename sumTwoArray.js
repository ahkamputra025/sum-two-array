let arr_1 = [3, 5, 22, 5,  7,  2,  45, 75, 89, 21, 2]; //output --> 276
let arr_2 = [9, 2, 42, 55, 71, 22, 4,  5,  90, 25, 26];  //output --> 351

const sumTwoArray = () => {
// write code here
  let arrlen1 = arr_1.length,
      arrlen2 = arr_2.length,
      sumArr1 = 0,
      sumArr2 = 0,
      result = 0;

  for(let i=0; i<arrlen1; i++){
    sumArr1 += arr_1[i];
  }
  for(let i=0; i<arrlen2; i++){
    sumArr2 += arr_2[i];
  }
  return result = sumArr1 + sumArr2;
}
// driver code
console.log(sumTwoArray(arr_1, arr_2)) // 627